# README #

This project was developed with Java EE servlet. It shows the integration and the deployment on google app engine, allowing the app to scale on multiple servers.
The Website is also optimized for mobile devices due to the Bootstrap Framework integration.

### Ready for mobile ###

With the bootstrap framework integration the website is also mobile-device compatible

### Known Bugs ###

If local deployed the csv file are exported correctly. The problem occours when the project is deployed to the
google app engine. Only the first address is stored in the csv file. Due to the lack of a appropriate console to debug the
software is not easy to trubleshoot this phenomen.