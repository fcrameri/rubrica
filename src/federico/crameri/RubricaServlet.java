package federico.crameri;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.ibm.hamlet.ExHamletHandler;
import com.ibm.hamlet.Hamlet;

public class RubricaServlet extends Hamlet {

	private static final long serialVersionUID = 5264892082158101750L;
	private static Logger logger = Logger.getLogger(RubricaServlet.class.getName());
	public static ArrayList<Address> addresses = new ArrayList<Address>();

	private void storeAddress(String firstname, String lastname, String street, String city, String state,
			String zipcode, String country, String phonenumber) {
		Key addressDataKey = KeyFactory.createKey("Addresses", "Rubrica");
		Entity sensorData = new Entity("Address", addressDataKey);
		sensorData.setProperty("firstname", firstname);
		sensorData.setProperty("lastname", lastname);
		sensorData.setProperty("street", street);
		sensorData.setProperty("city", city);
		sensorData.setProperty("state", state);
		sensorData.setProperty("zipcode", zipcode);
		sensorData.setProperty("country", country);
		sensorData.setProperty("phonenumber", phonenumber);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		datastore.put(sensorData);

		logger.debug("Address has been stored to app engine storage");
	} // storeAddress

	private void readAddress(String searchKey) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key addressDataKey = KeyFactory.createKey("Addresses", "Rubrica");
		Query query = new Query("Address", addressDataKey).addSort("firstname", Query.SortDirection.ASCENDING);
		if (searchKey != null) {
			query.setFilter(new Query.FilterPredicate("firstname", Query.FilterOperator.EQUAL, searchKey));
		}

		List<Entity> curAddresses = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(10));

		addresses.clear();

		for (Entity curAddress : curAddresses) {
			String firstname = "" + curAddress.getProperty("firstname");
			String lastname = "" + curAddress.getProperty("lastname");
			String street = "" + curAddress.getProperty("street");
			String city = "" + curAddress.getProperty("city");
			String state = "" + curAddress.getProperty("state");
			String zipcode = "" + curAddress.getProperty("zipcode");
			String country = "" + curAddress.getProperty("country");
			String phonenumber = "" + curAddress.getProperty("phonenumber");

			Address address = new Address(firstname, lastname, street, city, state, zipcode, country, phonenumber,
					curAddress);
			addresses.add(address);
		} // for
	} // readComments

	private void deleteAddress(int arrayPosition) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		datastore.delete(addresses.get(arrayPosition).entity.getKey());
	}

	public void init() {
		BasicConfigurator.configure();
		logger.debug("init");
	} // init

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		try {
			logger.debug("doPost has been executed");

			String firstname = req.getParameter("inputFirstname");
			String lastname = req.getParameter("inputLastname");
			String street = req.getParameter("inputStreet");
			String city = req.getParameter("inputCity");
			String state = req.getParameter("inputState");
			String zipcode = req.getParameter("inputZipcode");
			String country = req.getParameter("inputCountry");
			String phonenumber = req.getParameter("inputPhonenumber");

			String[] person = req.getParameterValues("address");

			if (firstname != null && lastname != null && street != null && city != null && state != null
					&& zipcode != null && country != null && phonenumber != null) {
				storeAddress(firstname, lastname, street, city, state, zipcode, country, phonenumber);
				logger.debug("new address object has been created");
			} // if
			else if (person[0] != "") {
				for (String element : person) {
					logger.debug("do Post delete personnnas: " + element);
					deleteAddress(Integer.parseInt(element));
				}

			} else {
				logger.debug("do Post without any parameters");
			}
			doGet(req, res);
		} catch (Exception e) {
			logger.error("", e);
		} // try

	} // doPost

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		try {
			String searchKey = req.getParameter("searchKey");
			logger.debug("search param::::::::::" + searchKey);
			logger.debug("doGet has been executed");
			readAddress(searchKey);
			// deleteAddress();
			ExHamletHandler handler = new RubricaHandler(this, addresses);
			serveDoc(req, res, "rubrica.html", handler);
		} catch (Exception e) {
			logger.error("", e);
		} // try
	} // doGet
}
