package federico.crameri;

import java.util.ArrayList;

import org.xml.sax.Attributes;

import com.ibm.hamlet.ExHamletHandler;
import com.ibm.hamlet.Hamlet;
import com.ibm.hamlet.helpers.Helpers;

public class RubricaHandler extends ExHamletHandler {
	private int i = 0 - 1;
	private ArrayList<Address> addresses;

	public RubricaHandler(Hamlet hamlet, ArrayList<Address> addresses) {
		super(hamlet);
		this.addresses = addresses;
	} // GuestBookHandler

	public int getIndirizziRepeatCount(String id, String name, Attributes atts) {
		i++;
		return addresses.size();
	} // getCommentsRepeatCount

	public String getFirstnameReplacement(String id, String name, Attributes atts) {
		return addresses.get(i).firstname.toString();
	} // getDateReplacement

	public String getLastnameReplacement(String id, String name, Attributes atts) {
		return addresses.get(i).lastname.toString();
	}

	public String getStreetReplacement(String id, String name, Attributes atts) {
		return addresses.get(i).street.toString();
	}

	public String getCityReplacement(String id, String name, Attributes atts) {
		return addresses.get(i).city.toString();
	}

	public String getStateReplacement(String id, String name, Attributes atts) {
		return addresses.get(i).state.toString();
	}

	public String getZipcodeReplacement(String id, String name, Attributes atts) {
		return addresses.get(i).zipcode.toString();
	}

	public String getCountryReplacement(String id, String name, Attributes atts) {
		return addresses.get(i).country.toString();
	}

	public String getPhonenumberReplacement(String id, String name, Attributes atts) {
		// return addresses.get(i - 1).entity.getKey().toString();
		return addresses.get(i).phonenumber.toString();
	}

	public Attributes getCheckboxAttributes(String id, String name, Attributes atts) throws Exception {
		atts = Helpers.getAttributes(atts, "value", "" + i);
		atts = Helpers.getAttributes(atts, "ID", "checkbox" + i);
		return atts;
	}

}
