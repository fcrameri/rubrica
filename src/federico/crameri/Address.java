package federico.crameri;

import java.io.Serializable;

import com.google.appengine.api.datastore.Entity;

public class Address implements Serializable {
	private static final long serialVersionUID = -1199467372679306131L;
	public String firstname;
	public String lastname;
	public String street;
	public String city;
	public String state;
	public String zipcode;
	public String country;
	public String phonenumber;
	public Entity entity;

	public Address(String firstname, String lastname, String street, String city, String state, String zipcode,
			String country, String phonenumber, Entity entity) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
		this.country = country;
		this.phonenumber = phonenumber;
		this.entity = entity;
	}
}
