package federico.crameri;

import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FileHandler extends HttpServlet {

	public void init() throws ServletException {
		// Do required initialization
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/csv");
		response.setHeader("Content-Disposition", "attachment; filename=\"Address.csv\"");

		for (Address element : RubricaServlet.addresses) {
			try {
				OutputStream outputStream = response.getOutputStream();
				String outputResult = element.firstname.toString();
				outputResult += ", " + element.lastname.toString();
				outputResult += ", " + element.street.toString();
				outputResult += ", " + element.city.toString();
				outputResult += ", " + element.state.toString();
				outputResult += ", " + element.zipcode.toString();
				outputResult += ", " + element.country.toString();
				outputResult += ", " + element.phonenumber.toString();

				outputStream.write(outputResult.getBytes());
				outputStream.write('\n');
				outputStream.flush();
				outputStream.close();
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			System.out.println(element.firstname.toString());
		}

	}
}
